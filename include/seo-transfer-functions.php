<?php

/**
 * Load pages defined in SEO transfer settings
 *
 * @param array $pages
 */
function lg_seo_load_contents_from_old_site($pages){

	foreach ($pages as $key => $value) {
		echo '<h4 style="color:red;"><span style="color:black;">From</span> ' . $value->old . ' <span style="color:black;">to</span> ' . $value->new. '</h4>';
		libxml_use_internal_errors(true);

		$html = lg_seo_file_get_contents($value->old);
		$dom = new domDocument;
		$dom->loadHTML($html);
		$dom = $dom->documentElement;
	
		$dom->preserveWhiteSpace = false;

		//Page Meta
		if($value->new){
			$page_title = $page_description = $page_keywords = '';

			$page_title = $dom->getElementsByTagName('title')->item(0)->nodeValue;
			$page_meta = $dom->getElementsByTagName('meta');
			for ($i = 0; $i < $page_meta->length; $i++)
			{
			    $meta = $page_meta->item($i);
			    if(strtolower($meta->getAttribute('name')) == 'description')
			        $page_description = $meta->getAttribute('content');
			    if(strtolower($meta->getAttribute('name')) == 'keywords')
			        $page_keywords = $meta->getAttribute('content');
			}
			lg_seo_page_meta_update($value->new, $page_title, $page_description, $page_keywords);

		}

		echo '<h4>IMAGES:</h4>';

		//Image Meta
		$images = $dom->getElementsByTagName('img');

		foreach ($images as $image) {
		  	$source =  $image->getAttribute('src');
		  	$source = explode("/", $source);
		  	$source = explode(".", end($source));

		  	if(sizeof($source) == 2){
			  	$filename = sanitize_title($source[0]);
			  	$extension = $source[1];
			  	$title = $image->getAttribute('title');
			  	$alt = $image->getAttribute('alt');
			  	$image_list = lg_seo_get_image_list_by_slug($filename);

			  	foreach ($image_list as $key => $value) {
			  		$url = $value->guid;
			  		$newImage = new SEOImage($filename, $extension, $url, $title, $alt, $value->ID);
			  		lg_seo_media_attachment_meta_update($newImage);
			  	}
		  	}
		}	
	}	
}

/**
 * Update page title, description and keywords into database through yoast.
 */
function lg_seo_page_meta_update($page_url, $title, $description, $keywords){
	$post_id = url_to_postid($page_url);

	if($post_id > 0){

		if($title && $title != ''){
			update_post_meta($post_id, '_yoast_wpseo_title', $title);
			echo '<b>PAGE TITLE</b>: "' . $title . '"<br>';
		}
		if($description && $description != ''){
			update_post_meta($post_id, '_yoast_wpseo_metadesc', $description);
			echo '<b>PAGE DESCRIPTION</b>: "' . $description . '"<br>';
		}
		if($keywords && $keywords != ''){
			update_post_meta($post_id, '_yoast_wpseo_focuskw', $keywords);
			echo '<b>PAGE KEYWORDS</b>: "' . $keywords . '"<br>';
		}
	}else{
		echo 'Post ' . $page_url . ' does not found.';
	}
}

/**
 *	Update title and alt texts for the images
 */
function lg_seo_media_attachment_meta_update($image){
	$title = $image->title;
	$alt = $image->alt;
	$updated_post = array(
	    'ID'           => $image->id,
	    'post_title'   => $title
	);

	wp_update_post( $updated_post );
	update_post_meta( $image->id, '_wp_attachment_image_alt', $alt );
	echo '<img style="max-width: 100px;" src="'. $image->url .'">';
	echo '<div style="margin-bottom: 20px;">';
	if($title){
		echo 'IMAGE TITLE: ' . $title . '<br>';
	}
	if($alt){
		echo 'IMAGE ALT: ' . $alt . '<br>';
	}
	echo '</div>';
}

/**
 *	Validate if images found on old url and new url are actually a match
 *	filename needs to be the same and as wordpress default behavior with upload multiple files with the same name
 *	same filename with "-" after still counts
 */
function lg_seo_validate_image_file( $images, $slug ){
	$return_list = [];

	foreach ($images as $key => $value) {
		$filename = $value->post_name;
		if(substr_count($filename, $slug) == 1 && lg_seo_start_with($filename, $slug)){
			$filename = str_replace($slug, '', $filename);
			if($filename == ''){
				array_push($return_list, $value);
			}else if(substr_count($filename, '-') == 1 && lg_seo_start_with($filename, '-')){
				$filename = str_replace('-', '', $filename);
				if(ctype_digit($filename)){
					array_push($return_list, $value);
				}
			}
		}
	}
	return $return_list;
}

function lg_seo_start_with($string, $target){
	return substr($string, 0, strlen($target)) === $target;
}

/**
 *	Wordpress default behavior with upload multiple files with the same name, remove the -1, -2, etc off the image filename to start a proper match
 */
function lg_seo_remove_thumbnail_attr( $slug ){
	$source = explode("-", $slug);

	if($source && is_array($source)){
		$last_string = end($source);

		if(is_numeric($last_string) && (int)$last_string < 100){
			array_pop($source);
		}

		$last_string = end($source);
		if(substr_count($last_string, 'x') == 1){
			array_pop($source);
		}
	}

	return join("-", $source);
}

/**
 *	Get images from the database by optimized filename
 */
function lg_seo_get_image_list_by_slug( $slug ) {
	global $wpdb;

	$slug = lg_seo_remove_thumbnail_attr($slug);

  	$querystr = "SELECT * FROM `".$wpdb->prefix."posts` WHERE post_type = 'attachment' AND post_name LIKE '" . $slug . "%'";
	$images = $wpdb->get_results($querystr, OBJECT);
	$images = lg_seo_validate_image_file($images, $slug);

  	return $images;
}

/**
 * Get Data
 *
 * Replace get_file_content()
 *
 * @param String  $String          - url
 *
 * @return String
 */
function lg_seo_file_get_contents($url){
  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}

?>