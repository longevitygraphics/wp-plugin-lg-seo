<?php
	class LGSEOAjax{

		function __construct(){
			add_action( 'wp_ajax_lg_seo_file', array( $this, 'lg_seo_file' ) );
			add_action( 'wp_ajax_lg_seo_transfer_upload', array( $this, 'lg_seo_transfer_upload' ) );
		}

		// Load site map xml url generate by yoast from the old website and return list of post type sitemaps url
		function lg_seo_file(){
			$data = json_decode(str_replace ('\"','"', $_REQUEST['data']));
			$xml=simplexml_load_file($data->url) or die("Error: Cannot create object");
			$result = array();
			foreach($xml->sitemap as $item)
			{
			   	array_push($result, (string)$item->loc);
			}
			echo json_encode($result);
			wp_die();
		}

		// Load all the post type urls from the sitemap and save to LG SEO settings
		function lg_seo_transfer_upload(){
			$data = json_decode(str_replace ('\"','"', $_REQUEST['data']))->data;
			$result = array();
			if($data && is_array($data)){
				foreach ($data as $key => $item) {
					
					$xml=simplexml_load_file($item) or die("Error: Cannot create object");
					foreach($xml->url as $item)
					{
					   	array_push($result, (string)$item->loc);
					}
				}
			}
			$result = array_map(array( $this, 'lg_seo_transfer_format_result' ),$result);
			update_field('lg_seo_transfer_site_pages', $result, 'option');
		}

		function lg_seo_transfer_format_result($item){
			return array('old_url'=>$item, 'new_url'=>'');
		}
	}
		

	new LGSEOAjax();
?>