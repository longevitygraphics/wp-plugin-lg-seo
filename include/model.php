<?php
	
	Class SEOImage{
		function __construct($filename, $extension, $url, $title, $alt, $id = NULL){
			$this->filename = $filename;
			$this->title = $title;
			$this->extension = $extension;
			$this->url = $url;
			$this->alt = $alt;
			$this->id = $id;
		}
	}

	Class SEOPage{
		function __construct($old, $new = NULL){
			$this->new = $new;
			$this->old = $old;
		}
	}

?>