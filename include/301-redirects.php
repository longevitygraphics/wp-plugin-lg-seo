<?php

function lg_seo_301_redirect_function() {
  $pages_field = get_field('lg_301_redirect_settings','option');

  if($pages_field && is_array($pages_field)){
    foreach ($pages_field as $key => $value) {
      $old_url = $value['target_url'];
      $new_url = $value['destination_url'];
      $current_url = explode("?",$_SERVER['REQUEST_URI'])[0];
      
      if($old_url === $current_url || $old_url . '/' === $current_url || '/' . $old_url . '/' === $current_url || '/' . $old_url === $current_url){
        wp_redirect($new_url,301);
        exit();
      }
    }
  }
}

add_action('template_redirect','lg_seo_301_redirect_function');
?>