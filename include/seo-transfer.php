<?php

require_once "model.php";
require_once "seo-transfer-functions.php";

$pages_field = get_field('lg_seo_transfer_site_pages','option');

$pages = [];

foreach ($pages_field as $key => $value) {
	$seoPage = new SEOPage($value['old_url'], $value['new_url']);
	array_push($pages, $seoPage);
}

echo '<h1>SEO Transfer</h1>';
if(lg_seo_transfer_dependencies_check()){
	lg_seo_load_contents_from_old_site($pages);
}else{
	echo '<p>Yoast Plugin does not found.</p>';
}



function lg_seo_transfer_dependencies_check() {
    if (!function_exists('get_plugins')) {
        require_once ABSPATH . 'wp-admin/includes/plugin.php';
    }
    $plugins = get_plugins();
    $plugin_found = false;

    if (isset($plugins['wordpress-seo/wp-seo.php']) || isset($plugins['wordpress-seo-premium/wp-seo-premium.php'])) {
    	
		if(is_plugin_active('wordpress-seo/wp-seo.php') || is_plugin_active('wordpress-seo-premium/wp-seo-premium.php')){
			$plugin_found = true;
		}
    }

	return $plugin_found;
}

?>