<?php

	function seo_transfer_fuctions(){
		require_once "seo-transfer.php";
	}

	function lg_seo_transfer_doc(){
		?>
			<div class="wrap">
				<br><hr><br>
				<h2>Documentation</h2>
				<p>This component built for Longevity Graphics to acommplish website SEO transfer for new projects. 
				It is capable of</p>
				<ul>
					<li>1. Transfer page titles, descriptions, keywords</li>
					<li>2. Image title, alts</li>
					<li>3. Page 301 redirects</li>
				</ul>
				<p>Contributor: Kelvin</p>
				<p>Contact: kelvin@longevitygraphics.com or xucongzhe@yahoo.com</p>
				<br><hr><br>
				<h3>Dependencies</h3>
				<ul>
					<li>1. Yoast SEO</li>
					<li>2. Advanced Custom Fields</li>
				</ul>
				<br><hr><br>
				<h3>Few things to notice</h3>
				<ul>
					<li>1. Make sure backup database before start to proceed.</li>
					<li>2. Images are search by filename between old site and new site. Please make sure no filename change when build the new site.</li>
					<li>3. If on the old site images are named as *-(int), for example test-1.jpg, test-2.jpg.  This does not work if there is a test.jpg or test.png etc. Because if wordpress found a duplication of fileupload, it will automantically append -1, -2 at the end of the slug for the later uploaded file. The component check the same thing.</li>
					<li>** Development notice: Make sure if you're using Image field in acf, leave the return type as 'Image array', which is the image object rather than just image url. And in php file you need to echo both image url and image alt.</li>
				</ul>
				<br><hr><br>
				<h3>Instructions</h3>
				<ol>
					<li>Initialize <a href="/wp-admin/admin.php?page=seo_transfer_settings">Settings</a>, and make sure everything is correct before proceed to next step. "**Go through all pages from the old site", found related coresponding page on the new site. If coresponding page on the new site does not exist. leave it empty.</li>
					<li>
						<span style="color:red">(optional) Import from sitemap from the old website.</span> <br>
						<input class="form-control" placeholder="xml file url" id="upload-file" type="text"><br>
						<select id="upload-file-entry" class="form-control" multiple></select>
						<a class="mt-2 btn btn-sm btn-white" id="upload-button">Update</a><br>
					</li>
					<li><a href="/wp-admin/admin.php?page=lg-seo-start-transfer">Start</a> transfer. **Make sure database is backed up</li>
				</ol>
			</div>

			<script>
				(function($) {
			
				    $(document).ready(function(){

				    	$('#upload-file').on('change', function(){
				    		var file = $(this).val();

			    		  	var data = {
								'action': 'lg_seo_file',
								'data': JSON.stringify({'url': file})
							};

							jQuery.post(ajaxurl, data, function(response) {
								var data = JSON.parse(response);
								$('#upload-file-entry').empty();

								for(var i = 0; i < data.length; i++){
									$('#upload-file-entry').append('<option value="'+data[i]+'">'+data[i]+'</option>');
								}
							});
				    	});

				    	$('#upload-button').on('click', function(){
				    		var data = $('#upload-file-entry').val();

				    		var data = {
								'action': 'lg_seo_transfer_upload',
								'data': JSON.stringify({'data': data})
							};

							jQuery.post(ajaxurl, data, function(response) {
								alert('Updated');
							});
				    	});
				      
				    });

				}(jQuery));
			</script>
		<?php
	}

	function lg_seo_transfer_menu() {

		add_menu_page(
	        __( 'LG SEO Transfer Documentation', 'lg-seo-transfer' ),
	        'LG SEO',
	        'manage_options',
	        'lg-seo-transfer',
	        'lg_seo_transfer_doc',
	        'dashicons-networking',
	        3
	    );

        acf_add_options_page(array(
			'page_title' 	=> 'Settings',
			'menu_title' 	=> 'Settings',
			'menu_slug' 	=> 'seo_transfer_settings',
			'parent_slug'   => 'lg-seo-transfer',
			'position'		=> 9
		));

		add_submenu_page(
            '', 
            'Start Transfer', 
            'Start Transfer', 
            'manage_options', 
            'lg-seo-start-transfer',
            'seo_transfer_fuctions'
        );

    }

	add_action('init', 'lg_seo_transfer_menu');
?>