<?php

/**
* Plugin Name: LG SEO Transfer
* Plugin URI: https://www.longevitygraphics.com/
* Author: Kelvin Xu,
* Version: 2.2
*/

	function lg_seo_init(){
		require_once 'include/seo-transfer-functions.php';
		require_once 'include/menu.php';
		require_once 'include/acf/lg-option-seo-transfer-acf.php';
		require_once 'include/301-redirects.php';
		require_once 'include/ajax.php';
	}

	function lg_seo_admin_notice__error($message) {
		$class = 'notice notice-error';

		printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) ); 
	}

	function lg_seo_plugin_dependencies_check() {
	    if (!function_exists('get_plugins')) {
	        require_once ABSPATH . 'wp-admin/includes/plugin.php';
	    }
	    $plugins = get_plugins();
	    $error_message = '';

	    if (isset($plugins['advanced-custom-fields-pro/acf.php'])) {
	    	
			if(!is_plugin_active('advanced-custom-fields-pro/acf.php')){
				$error_message .= 'LG Plugin : ACF Pro is not activated.';
			}
	    }else{
	    	$error_message .= 'LG Plugin : ACF PRO is not detected';
	    }

		if($error_message == ''){
			add_action( 'plugins_loaded', 'lg_seo_init' );
		}else{
			add_action( 'admin_notices', function() use ($error_message) {
				$class = 'notice notice-error';
				lg_seo_admin_notice__error($error_message);
			}
		 );
		}
	}

	lg_seo_plugin_dependencies_check();
?>